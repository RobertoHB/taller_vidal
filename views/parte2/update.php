<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Parte2 */

$this->title = 'Update Parte2: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Parte2s', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="parte2-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
