<?php

/* @var $this yii\web\View */
use yii\helpers\Url;
//$this->title = 'Taller Mecaníco J. Vidal';
?>

<head>
<style type="text/css">
		body{
			background-color: #333167;
			color: white;
			font-weight: bold;
                        margin-top:200px;
		}
		td{
			text-align: center;
		}
		button{
			background-color: #333167;
		}
                a{
                    color:white;
                    font-weight: bold;
                    text-decoration: none;
                }
                a:hover{
                    color:white;
                     font-weight: bold;
                    text-decoration: none;
                }
	</style>
</head>
<body>
	<table style="width: 100%;">
		
		
            <tr>
			<td width="20%"></td>
			<td></td>
                        <td><a href="<?= url::to('@web/clientes')?>"><img src="<?= Url::to('@web/img/clientes.png')?>" alt="Clientes" width="100" height="100">
				<h4>CLIENTES</h4></a></td>
			<td></td>
			<td><a href="<?= url::to('@web/vehiculos')?>"><img src="<?= Url::to('@web/img/vehiculos.png')?>" alt="Vehiculos" width="100" height="100">
				<h4>VEHÍCULOS</h4></a></td>
			<td></td>
			<td width="20%"></td>
		</tr>
		<tr>
			<td width="20%"></td>
			<td><a href="<?= url::to('@web/parte1')?>"><img src="<?= Url::to('@web/img/reparaciones.png')?>" alt="Reparaciones" width="100" height="100"></i>
				<h4>REPARACIONES</h4></a></td>
			<td></td>
			<td><a href="<?= url::to('@web/repuestos')?>"><img src="<?= Url::to('@web/img/repuestos.png')?>" alt="Repuestos" width="100" height="100">
				<h4>REPUESTOS</h4></a></td>
			<td></td>
			<td><a href="<?= url::to('@web/site/informes')?>"><img src="<?= Url::to('@web/img/informes.png')?>" alt="Informes" width="100" height="100">
				<h4>INFORMES</h4></a></td>
			<td width="20%"></td>
		</tr>
	</table>
</body>


