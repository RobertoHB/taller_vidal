<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "clientes".
 *
 * @property int $id
 * @property string|null $nombre
 * @property string|null $apellidos
 * @property string|null $rs
 * @property string|null $cif_nif
 * @property string|null $direccion
 * @property int|null $cp
 * @property string|null $localidad
 * @property string|null $provincia
 * @property string|null $email
 * @property int|null $fijo
 * @property int|null $movil
 *
 * @property Vehiculos[] $vehiculos
 */
class clientes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'clientes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cp', 'fijo', 'movil'], 'integer'],
            [['nombre', 'apellidos', 'rs'], 'string', 'max' => 100],
            [['cif_nif'], 'string', 'max' => 15],
            [['direccion'], 'string', 'max' => 150],
            [['localidad', 'provincia', 'email'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'apellidos' => 'Apellidos',
            'rs' => 'Rs',
            'cif_nif' => 'Cif Nif',
            'direccion' => 'Direccion',
            'cp' => 'Cp',
            'localidad' => 'Localidad',
            'provincia' => 'Provincia',
            'email' => 'Email',
            'fijo' => 'Fijo',
            'movil' => 'Movil',
        ];
    }

    /**
     * Gets query for [[Vehiculos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getVehiculos()
    {
        return $this->hasMany(Vehiculos::className(), ['cliente' => 'id']);
    }
    
    
    public function getCte_tiene_partes($id)
    {
       
               $consulta = clientes::find()
                ->from('clientes c')   
                 ->innerJoin('vehiculos v', 'c.id = v.cliente')
                 ->innerJoin('parte1 p1', 'p1.vehiculo = v.id')
                 ->where(['c.id'=>$id])
                ->asArray()
                ->all();
             if(count($consulta)>0){
                  return true;
             }else{                 
                 return false;
             }
    }
}
