<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Vehiculos;

/**
 * VehiculosSearch represents the model behind the search form of `app\models\Vehiculos`.
 */
class VehiculosSearch extends Vehiculos
{
    public $filtro_nombre;
    public $filtro_apellidos;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'cliente'], 'integer'],
            [['matricula', 'marca', 'color', 'bastidor', 'combustible','filtro_nombre','filtro_apellidos'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Vehiculos::find()->joinWith(['cliente0']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        
          $dataProvider->setSort([
        'attributes' => [
            'id',
            'cliente',
            'filtro_nombre', 
            'filtro_apellidos', 
            'maticula',
            'marca',
            'color',
            'bastidor',
            'combustible',
            
            ]
        ]);
        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'cliente' => $this->cliente,
        ]);

        $query->andFilterWhere(['like', 'matricula', $this->matricula])
            ->andFilterWhere(['like', 'marca', $this->marca])
            ->andFilterWhere(['like', 'color', $this->color])
            ->andFilterWhere(['like', 'bastidor', $this->bastidor])
            ->andFilterWhere(['like', 'combustible', $this->combustible])
            ->andFilterWhere(['like', 'clientes.nombre', $this->filtro_nombre])
            ->andFilterWhere(['like', 'clientes.apellidos', $this->filtro_apellidos]);
       
         
        return $dataProvider;
    }
}
