<?php

namespace app\models;

use Yii;
use app\models\Parte1;

/**
 * This is the model class for table "vehiculos".
 *
 * @property int $id
 * @property int|null $cliente
 * @property string|null $matricula
 * @property string|null $marca
 * @property string|null $color
 * @property string|null $bastidor
 * @property string|null $combustible
 *
 * @property Parte1[] $parte1s
 * @property Clientes $cliente0
 */
class vehiculos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vehiculos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cliente'], 'integer'],
            [['matricula'], 'string', 'max' => 10],
            [['marca', 'combustible'], 'string', 'max' => 25],
            [['color'], 'string', 'max' => 20],
            [['bastidor'], 'string', 'max' => 17],
            [['cliente'], 'exist', 'skipOnError' => true, 'targetClass' => Clientes::className(), 'targetAttribute' => ['cliente' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'cliente' => 'Cliente',
            'matricula' => 'Matricula',
            'marca' => 'Marca',
            'color' => 'Color',
            'bastidor' => 'Bastidor',
            'combustible' => 'Combustible',
        ];
    }

    /**
     * Gets query for [[Parte1s]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getParte1s()
    {
        return $this->hasMany(Parte1::className(), ['vehiculo' => 'id']);
    }

    /**
     * Gets query for [[Cliente0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCliente0()
    {
        return $this->hasOne(Clientes::className(), ['id' => 'cliente']);
    }
    
     public function getDuplicada($matricula)
    {
         if(vehiculos::findOne(['matricula'=>$matricula])){
            return true;
        }else{
            return false;
        }
    }
    public function getTiene_partes($vehiculo)
    {
         if(parte1::findOne(['vehiculo'=>$vehiculo])){
            return true;
        }else{
            return false;
        }
    }
}
