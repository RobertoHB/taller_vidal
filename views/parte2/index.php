<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\Parte2Search */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Parte2s';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="parte2-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Parte2', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'id_parte1',
            'codigo',
            'descripcion',
            'cantidad',
            //'dto',
            //'importe',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
