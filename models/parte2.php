<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "parte2".
 *
 * @property int $id
 * @property int|null $id_parte1
 * @property string|null $codigo
 * @property string|null $descripcion
 * @property int|null $cantidad
 * @property int|null $dto
 * @property float|null $importe
 *
 * @property Parte1 $parte1
 */
class parte2 extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'parte2';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_parte1', 'cantidad', 'dto'], 'integer'],
            [['importe'], 'number'],
            [['codigo'], 'string', 'max' => 10],
            [['descripcion'], 'string', 'max' => 200],
            [['id_parte1'], 'exist', 'skipOnError' => true, 'targetClass' => Parte1::className(), 'targetAttribute' => ['id_parte1' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_parte1' => 'Id Parte1',
            'codigo' => 'Codigo',
            'descripcion' => 'Descripcion',
            'cantidad' => 'Cantidad',
            'dto' => 'Dto',
            'importe' => 'Importe',
        ];
    }

    /**
     * Gets query for [[Parte1]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getParte1()
    {
        return $this->hasOne(Parte1::className(), ['id' => 'id_parte1']);
    }
}
