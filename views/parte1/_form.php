<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\bootstrap\Modal;
use yii\jui\AutoComplete;
use yii\web\JsExpression;
use app\models\clientes;
use app\models\vehiculos;
//use dosamigos\datetimepicker\DateTimePicker;
use dosamigos\datepicker\DatePicker;
/* @var $this yii\web\View */
/* @var $model app\models\Parte1 */
/* @var $form yii\widgets\ActiveForm */
//setlocale(LC_TIME,"es_ES");
$estado_presupuesto = [0 => 'Pendiente', 1 => 'Aceptado']; 

////$datajson = Json::decode($datos_autoc);
//$data =  ArrayHelper::toArray($datos_autoc); 
//var_dump($datos_autoc);
?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.0/jquery-ui.min.js" integrity="sha256-eGE6blurk5sHj+rmkfsGYeKyZx3M4bG+ZlFyA7Kns7E=" crossorigin="anonymous"></script>
<div class="parte1-form">
   <div class="container"> 
        <!--<div class="row">-->
            <?php $form = ActiveForm::begin(); ?>
<!--            <form action="/hms/accommodations" method="GET"> -->
            <div class="row" id="buscador-autocomplete" style="display:none">
                <div class="col-xs-6 col-md-12">
                    <div class="input-group"><label style="margin-right: 15px;">Buscar Vehiculo/Cliente  </label>
                   
                    <?= AutoComplete::widget([  
                        "id" => "auto",
                        'clientOptions' => [
                        'source' => $datos_autoc,
                        'minLength'=>'3', 
                        'autoFill'=>true,
                        'select' => new JsExpression("function( event, ui ) {
                                                $('#vehiculo').val(ui.item.id);
                                                  var vehiculo = ui.item.id;
                                                  datoscabecera(vehiculo);
                                                 
                                                //#memberssearch-family_name_id is the id of hiddenInput.
                                             }")],
                         'options' => ['style'=>'width: 400px;border-radius:5px;background-color:#f5f5f5;border-color:#ccc']
                                             ]);
            ?> <hr>
                    <div class="input-group-btn">
                        
                    </div>
                  </div>
                </div><!--
                <div class="col-xs-6 col-md-5">
                 <div class="input-group">
                   <input type="text" class="form-control" placeholder="Buscar" id="txtSearch"/>
                   <div class="input-group-btn">
                     <button class="btn btn-primary" type="buton ">
                       <span class="glyphicon glyphicon-search"></span>
                     </button>
                   </div>
                 </div>
               </div>  -->
                  
                  
              </div>
            <!--</form>-->
            
        <!--</div>-->
       <div class="row">
           <div class="col-xs-6 col-sm-3 col-md-3" style="border-radius:25px;border:1px solid;padding:15px 15px 15px 15px;margin: 20px 80px 20px 20px;border-color: #286092">
                <?= Html::input('text', 'nombre', 'Cliente', ['class' => 'form-control','id'=> 'nombre']) ?> 
                <!--Html::input('text', 'direccion', 'Direccion', ['class' => 'form-control','id'=> 'direccion'])-->  
                <?= Html::input('text', 'poblacion', 'Poblacion', ['class' => 'form-control','id'=> 'poblacion']) ?> 
                <?= Html::input('text', 'cif_dni', 'CIF/DNI', ['class' => 'form-control','id'=> 'cif_dni']) ?> 
           </div>
           <div class="col-xs-6 col-sm-3 col-md-3" style="border-radius:25px;border:1px solid;padding:15px 15px 15px 15px;margin: 20px 80px 20px 20px;border-color: #286092">
                <?= Html::input('text', 'marca', 'Marca/Modelo', ['class' => 'form-control','id'=> 'marca']) ?> 
                <?= Html::input('text', 'matricula', 'Matricula', ['class' => 'form-control','id'=> 'matricula']) ?> 
                <?= Html::input('text', 'bastidor', 'Bastidor', ['class' => 'form-control','id'=> 'bastidor']) ?>  
              
           </div>
            <div class="col-xs-6 col-sm-3 col-md-3" style="border-radius:25px;border:1px solid;padding:-10px 15px 15px 15px;margin: 20px 10px 10px 10px;border-color: #286092;">
                 <span style="font-weight:bold;font-size: 12px;">Parte</span>
                <?= $form->field($model, 'id')->textInput(['style'=>'margin-top:-10px;display:none','placeholder'=>'Parte Nº','id'=>'parte'])->label('') ?>
               <span style="font-weight:bold;font-size: 12px">Factura</span>
                <?= $form->field($model, 'nparte')->textInput(['style'=>'margin-top:-75px;','placeholder'=>'Parte Nº','id'=>'parte_prov'])->label('') ?>
                
                <?= $form->field($model, 'nfactura')->textInput(['style'=>'margin-top:-32px;','placeholder'=>'Factura Nº','id'=>'factura'])->label('') ?>
              
           </div>
         </div>  
        <div class="row" >
            <div class="col-xs-6 col-sm-3 col-md-3">
              <?= $form->field($model, 'entrada')->widget(DatePicker::className(), [
                'language' => 'es',
                //'size' => 'ms',
               // 'template' => '{input}',
                
                'inline' => false,
                'clientOptions' => [
//                    'startView' => 1,
//                    'minView' => 0,
//                    'maxView' => 1,
                    'autoclose' => true,
                     'format' => 'dd-mm-yyyy',
                    'clearBtn' => true,
                    //'format' => 'yyyy-m-d',
                    //'linkFormat' => 'HH:ii P', // if inline = true
                    // 'format' => 'HH:ii P', // if inline = false
                    'todayBtn' => true
                ]
            ]);?>
               
           </div>
           <div class="col-xs-6 col-sm-3 col-md-3">
              <?= $form->field($model, 'salida')->widget(DatePicker::className(), [
                'language' => 'es',
                //'size' => 'ms',
               // 'template' => '{input}',
                
                'inline' => false,
                'clientOptions' => [
//                    'startView' => 1,
//                    'minView' => 0,
//                    'maxView' => 1,
                    'autoclose' => true,
                     'format' => 'dd-mm-yyyy',
                    'clearBtn' => true,
                    //'format' => 'yyyy-m-d',
                    //'linkFormat' => 'HH:ii P', // if inline = true
                    // 'format' => 'HH:ii P', // if inline = false
                    'todayBtn' => true
                ]
            ]);?>
             
           </div>
           <div class="col-xs-6 col-sm-2 col-md-2">
              
                 <?= $form->field($model, 'estado')->dropDownList($estado_presupuesto, ['placeholder'=>'Estado'])->label('Estado') ?>
           </div>
           <div class="col-xs-6 col-sm-2 col-md-2">
             <!--$form->field($model, 'kms')->widget(\yii\widgets\MaskedInput::className(), ['mask' => '999.999','class' => 'form-control'])->label('Kms') ?>-->
              <?= $form->field($model, 'kms')->textInput(['class' => 'form-control','placeholder'=>'Kms'])->label('Kms') ?>
           </div>
            <div class="col-xs-6 col-sm-1 col-md-1">
                <?= $form->field($model, 'dto')->textInput(['class' => 'form-control','placeholder'=>'Dto','id'=>'dto','style'=>'text-align:center'])->label('% Dto') ?>
          
           </div>
            <div class="col-xs-6 col-sm-1 col-md-1">
                <?= $form->field($model, 'iva')->textInput(['value'=>'21','class' => 'form-control','placeholder'=>'IVA','id'=>'iva','style'=>'text-align:center'])->label('% Iva') ?>
             
           </div>
        </div>   
                <?= (isset($_REQUEST['vehiculo'])) ? $form->field($model, 'vehiculo')->textInput(['value'=>$_REQUEST['vehiculo'],'id'=>'vehiculo','style'=>'display:none'])->label(''):
                    $form->field($model, 'vehiculo')->textInput(['style'=>'display:none','id'=>'vehiculo'])->label('') ?>
         <div class="row">
            <div class="col-sm-12">
                <?= $form->field($model, 'descripcion')->textarea(['rows' => 2,'style'=>'margin-top:-45px','placeholder'=>'Descripción de la reparación'])->label('') ?>
            </div>
        </div>
       
    </div>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Guardar') : Yii::t('app', 'Actualizar'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary','id'=>'envioForm','style'=>'width:120px;']) ?>
        <?= Html::a('Imprimir', ['site/pdf_parte', 'parte' => $model->id], ['class' => 'btn btn-primary','id'=>'imprimir_parte','style'=>'width:120px;','target'=>'_blank']) ?>
    </div>
    

    <?php ActiveForm::end(); ?>

</div>
<hr>
<h4>Detalle de la Reparación</h4>
<div class="form-inline" id="form_detalle">
    <div class="row">
        <div class="col-xs-10 col-sm-11">
    <!--    <p id='msjs_form' style='text-align: center;background-color: #F5F5F5;width: 500px;margin-left: 300px;font-weight: bold;border-radius: 40px;'></p>-->
            <?= Html::input('text', 'idparte', Yii::$app->request->post('idparte'), ['class' => 'form-control','id'=> 'idparte','style'=>'display:none']) ?>
            <?= Html::input('text', 'repuesto', Yii::$app->request->post('repuesto'), ['class' => 'form-control','placeholder'=>'Repuesto','id'=> 'repuesto']) ?>
            <?= Html::input('text', 'descripcion', Yii::$app->request->post('descripcion'), ['class' => 'form-control','placeholder'=>'Descripcion','id'=> 'descripcion']) ?>
            <?= Html::input('text', 'cantidad', Yii::$app->request->post('cantidad'), ['class' => 'form-control','placeholder'=>'Cantidad','id'=> 'cantidad']) ?>
            <?= Html::input('text', 'dto_int', Yii::$app->request->post('dto'), ['class' => 'form-control','placeholder'=>'Dto','id'=> 'dto_int']) ?>
            <?= Html::input('text', 'importe', Yii::$app->request->post('importe'), ['class' => 'form-control','placeholder'=>'importe','id'=> 'importe']) ?>
        </div>
        <div class="col-xs-2 col-sm-1">
            <?= Html::Button('Añadir', ['class' => 'btn btn-primary', 'name' => 'nuevo_detalle','id' => 'nuevo_detalle','style'=>'width:100px;margin-left:-20px;']) ?>
         </div>
    </div>
</div>
<div id="mygrid" style="margin-top: 15px;">
<?php Pjax::begin(['id'=>'detalle-pjax']);
    $base = 0;
    foreach ($dataProvider->models as $model) {
        $base += ($model->cantidad * $model->importe)-(($model->cantidad * $model->importe)*(intval($model->dto)/100));
    }
//    $subtotal_pie = $base *($_REQUEST['dto']/100);
//    $iva = $subtotal_pie *($_REQUEST['iva']/100);
//    $total_factura = $subtotal_pie + $iva;
?>
    
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
//        'id' => 'myGrid',
        // 'showFooter' => true,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],
            //'id_parte1'
            [
             'label'=>'Código.',
             'attribute'=>'codigo',
             'contentOptions' => ['style' => 'width:175px'],
            ],
            [
             'label'=>'Descripción',
             'attribute'=>'descripcion',
             'contentOptions' => ['style' => 'width:250px'],
            ],
            //'codigo',
            //'descripcion',
             [
             'label'=>'Un.',
             'attribute'=>'cantidad',
             'headerOptions' => ['style' => 'text-align:center'],
             'contentOptions' => ['style' => 'text-align:right;width:40px'],
            ],
            [
             'label'=>'Dto',
             'attribute'=>'dto',
             'format'=>'raw',
             //'headerOptions' => ['style'=>'text-align:center'],
             'contentOptions' => function ($model) {
                             return ['style' => 'text-align:center;color:' 
                                 .($model->dto > 0 ? '#337ab7' : '')];
                            },  
             'headerOptions' => ['style' => 'width:10px;text-align:center'],
             
             //'contentOptions' => [0=>['style'=>'color:red'],1=>['style'=>'color:blue']],   
             'value' => function($model) {
                            return $model->dto == '' ? '' : $model->dto;}             
            ],
          // 'dto',
            [
            'label' => 'Importe',
            'attribute' => 'importe', 
            'headerOptions' => ['style' => 'text-align:center'],
            'contentOptions' => ['style' => 'text-align:right;width:70px'],
            'format'=>['decimal',2],
            ],
           [
            'label' => 'Subtotal',
            'value' => function ($model){
                      return ($model->cantidad * $model->importe)-(($model->cantidad * $model->importe)*(intval($model->dto)/100));
                 },
            'contentOptions' => ['style' => 'text-align:right;width:100px'],
            'format'=>['decimal',2],
            'headerOptions' => ['style' => 'text-align:center;color:#337ab7;'],
               //'footer' => '<p>BASE:'.$base.'</p><p>DTO. %:</p><p>SUBTOTAL:</P><P>I.V.A. 21 %:</p><p>TOTAL:<p>',
            ],
             
            ['class' => 'yii\grid\ActionColumn',
                
            'contentOptions' => ['style' => 'width:100px;text-align:center'],
            'header'=>'Opciones',
            'headerOptions' => ['style' => 'text-align:center'],
            'template' => '{update}{delete}',
            'buttons' => [
                             'update' => function ($url,$model,$key) {
                                                return Html::button(
                                                '<span class="glyphicon glyphicon-pencil" style="font-size:20px;"></span>',['value' =>url::to('@web/parte2/updatemodal'.'?id='.$model->id),'id'=>'modalBtnUpdate','style'=>'border:none;background-color:transparent;']);
                                                       
//                                                    ['data'=>[
//                                                                'method' => 'get',
//                                                                'params'=>['id'=>$model->id],                                         
//                                                                ]
//                                                    ]
//                                                        );
                                },
                            'delete' => function ($url, $model, $key) {
                                return  Html::a(
                                '<span class="glyphicon glyphicon-trash" style="padding-left:5px;font-size:20px"></span>',Url::to('@web/parte2/eliminar_pjax'.'?id='.$model->id),                                                                
                                                         ['data'=>[
                                                                'method' => 'post',
                                                                'confirm' => 'Are you sure?',
                                                                'params'=>['parte'=>$model->id_parte1],
                                                                ]
                                                        ]);
  
                            }
                        ]
            ],  

        ],
    ]); ?>
    
     
<?php Pjax::end(); ?>
    
    
     <!--ventana modal para actualizar vehiculos-->
<div id="modal_actualizar_modulo">
    <?php Modal::begin([
        'id'=>'modal_act_detalle_parte',
        'size'=>'modal-lg',
        'header' => '<h2>Actualizacion de detalle reparacion </h2>',
            //'toggleButton' => ['label' => 'click me'],
    ]);

        echo "<div id='modalContent'></div>";

    Modal::end();?>
</div>
     
     <div class="row">
        <div class="col-xs-2 col-sm-2" style="border:solid 1px grey;border-radius: 5px;margin-left:50px" align="center">
            <span style="color:#2398D6;font-weight: bold;" >Base: </span>
            <span  id ="base" value ="<?=$base?>" style="text-align:center"><?=$base?></span><span>€</span>
           <!--Html::input('text', 'base',$base, ['class' => 'form-control','id'=>'base','style'=>'text-align:center']) ?>-->
        </div>
        <div class="col-xs-2 col-sm-2" style="border:solid 1px grey;border-radius: 5px;margin-left:15px" align="center">
            <span style="color:#2398D6;font-weight: bold;">Dto %: </span>
            <span id ="dto_pie" style="text-align:center"></span>
            <!--Html::input('text', 'dto_pie','', ['class' => 'form-control','id'=>'dto_pie','style'=>'text-align:center'])--> 
        </div>
        <div class="col-xs-2 col-sm-2" style="border:solid 1px grey;border-radius: 5px;margin-left:15px" align="center">
            <span style="color:#2398D6;font-weight: bold;">Subtotal: </span>
            <span id ="subtotal_pie" style="text-align:center"></span>
             <!--Html::input('text', 'subtotal','', ['class' => 'form-control','id'=>'subtotal_pie','style'=>'text-align:center']) ?>-->
        </div>
        <div class="col-xs-2 col-sm-2" style="border:solid 1px grey;border-radius: 5px;margin-left:15px" align="center">
            <span style="color:#2398D6;font-weight: bold;">I.V.A.: </span>
            <span id ="iva_pie" style="text-align:center"></span>
           <!--Html::input('text', 'iva_pie','', ['class' => 'form-control','id'=>'iva_pie','style'=>'text-align:center']) ?>-->
        </div>
        <div class="col-xs-2 col-sm-2" style="border:solid 1px grey;border-radius: 5px;margin-left:15px" align="center">
            <span style="color:#2398D6;font-weight: bold;">TOTAL: </span>
            <span id ="total_factura" style="text-align:center"></span>
            <!--Html::input('text', 'total_factura','', ['class' => 'form-control','id'=>'total_factura','style'=>'text-align:center']) ?>-->
        </div>
    </div>
   
</div>


<script>
$( document ).ready(function() {
//     console.log( $('#codigo_cliente').html());
  
  
    if ( $('#vehiculo').val() >0 ){ 
        var vehiculo = $('#vehiculo').val();
        datoscabecera(vehiculo);
    }else{
        $('#buscador-autocomplete').show();
    }
 
  
    if ( $('#parte').val() != '' ){  
        $('#parte_prov').val($('#parte').val());  
        $('#idparte').val($('#parte').val());  
    }
    
   
    
    //vamos calculando los totales al pie del gridview
    var base = parseFloat($('#base').text()).toFixed(2);
    var dto = $('#dto').val();
    var iva = $('#iva').val();           
    var tot_dto = parseFloat(base *(dto/100)).toFixed(2);
    var subtotal = parseFloat(base -  tot_dto).toFixed(2);
    var tot_iva = parseFloat((subtotal * iva)/100).toFixed(2);
    var total_factura = parseFloat(parseFloat(subtotal) + parseFloat(tot_iva)).toFixed(2);
    $('#base').text(base);
    $('#dto_pie').text(tot_dto+' €');
    $('#subtotal_pie').text(subtotal+' €');
    $('#iva_pie').text(tot_iva+' €');
    $('#total_factura').text(total_factura+' €');
    
    
    $(document).on('pjax:error', function(event, xhr) {
         $('#mensaje').html('Datos introducidos erroneos o linea duplicada');  
//        alert('Pjax failed!');
//        console.log(xhr.responseText);
//        event.preventDefault();
    });
        $('body').on('click', '#auto', function() {
           $('#vehiculo').val('');
        });
     
        
      $("#nuevo_detalle").click(function(){
          insertarLinea();
      });
      
      
      $(document).on("click", "#modalBtnUpdate", function () {
        //console.log("mostrar modal")
         $('#modal_act_detalle_parte').modal('show')
            .find('#modalContent')
            .load($(this).attr('value'));
     });
      
   

        if($('#envioForm').hasClass("btn btn-success")){
            //console.log("parte nuevo");
            $('#form_detalle div').children().prop('disabled', true);
        }      
        
}); 


    function insertarLinea(){    
        
        var baseUrl = "<?= Url::toRoute(["/"]); ?>"; 
          $.ajax({

            type: "POST",
            url: baseUrl + "parte2/detalle_parte",

            data: {
              idparte: $("#idparte").val(),
              repuesto: $("#repuesto").val(),
              descripcion: $("#descripcion").val(),
              cantidad: $("#cantidad").val(),
              dto: $("#dto_int").val(),
              importe: $("#importe").val(),

            }

          })
        .done(function (data) {
            $.pjax.reload({container:"#detalle-pjax",timeout: false});
            $.pjax.reload({container:"#mygrid",timeout: false});
        });
    }
    function datoscabecera(vehiculo){    
        var vehiculo = vehiculo;
        var baseUrl = "<?= Url::toRoute(["/"]); ?>"; 
          $.ajax({

              type: "get",
              url: baseUrl + "vehiculos/datos_cabecera",

              data: {
                vehiculo: vehiculo,

              }
          })
               .done(function (data) {
                var obj = JSON.parse(data);
                $.each( obj, function( key, value ) {  
                    $('#nombre').val(value['nombre']+" "+ value['apellidos']);  
                    $('#poblacion').val(value['localidad']);
                    $('#cif_dni').val(value['cif']);
                    $('#marca').val(value['marca']);
                    $('#matricula').val(value['matricula']);
                    $('#bastidor').val(value['bastidor']);
                      
                });
//                
//                 document.write(obj.nombre);
                   
          });
    }
    
</script>
