<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\data\SqlDataProvider;
use \Mpdf\Mpdf;
use \yii\helpers\Url;
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
     public function actionInformes(){
         return $this->render('informes');
     }
    
    
    
    public function actionPdf_parte($parte){
       
          if(isset($parte)){
            $datos = new SqlDataProvider([
                'sql' => "SELECT p.id parte,p.vehiculo vehiculo,p.entrada entrada, p.salida salida, 
                                p.nfactura factura, p.descripcion descripcion, p.estado estado ,p.kms kms,p.dto dto,p.iva iva,
                                 v.cliente cliente, v.matricula matricula, v.marca marca, v.color color, v.bastidor bastidor, v.combustible combustible,
                                 c.nombre nombre,c.apellidos apellidos,c.direccion direccion,c.cp cp,c.localidad localidad,c.cif_nif cifnif
                                 FROM parte1 p JOIN vehiculos v ON p.vehiculo = v.id 
                                                    join clientes c ON v.cliente = c.id 
                                                   WHERE p.id = $parte",   
           ]); 
         
  
            $resultado = $datos->getModels();
            
           //Datos cabecera cliente
            $factura =  $resultado[0]['factura'];
            $nombre =  $resultado[0]['nombre'];
            $apellidos =  $resultado[0]['apellidos'];
            $direccion =  $resultado[0]['direccion'];
            $localidad =  $resultado[0]['cp']." ".$resultado[0]['localidad'];
            $cif_nif =  $resultado[0]['cifnif'];
            
            
            //Datos de cabecera del Vehiculo
             $marca =  $resultado[0]['marca'];
             $matricula =  $resultado[0]['matricula'];
             $kms =  $resultado[0]['kms'];
             $bastidor =  $resultado[0]['bastidor'];
            
            //Datos parte reparacion
              $entrada =  $resultado[0]['entrada'];
              $salida =  $resultado[0]['salida'];
              $iva = $resultado[0]['iva'];
              $dto = $resultado[0]['dto'];
              $descripcion =  $resultado[0]['descripcion'];
            
            
            //Datos del detalle de la reparacion
             $detalle_parte = new SqlDataProvider([
                'sql' => "SELECT pd.id_parte1 parte,pd.codigo codigo,pd.descripcion desc_repuesto,pd.cantidad cantidad,
                                pd.dto dto, pd.importe importe
                    FROM parte2 pd WHERE pd.id_parte1 = '$parte'",   
           ]); 
            $resultado_detalle = $detalle_parte->getModels();
            
            
          
            //logo taller
            $logo = Url::to('@web/img/logo.png');
            //$logo_gob =  url::to('@web/img/gobcantabria.jpg');
 
            
        }
            
        
   
   
$contenido = '
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	
	
</head>
<body>
	<table id="sinbordes">
		<tr>
			<td colspan="4" id="sinbordes"><img src="'.$logo.'" alt="Taller J. Vidal" width="240px"></td>
			<td colspan="3" id="sinbordes" align="right">
				<p id="cabecera"><b>JOSE ISIDORO VIDAL<br>
				Barrio La Trapa, 9<br>
				39718 - Medio Cudeyo<br>
				CIF: 13768925K<br>
				TELF: 942522410</b></p>
			</td>
		</tr>
	</table>
	<table>
		<tr>
			<td colspan="5">
				<h3>FACTURA  '.$factura.'</h3>
				<p><b>Nombre: </b>'.$nombre." ".$apellidos.'</p>
                                <p><b>Dirección: </b>'.$direccion.'</p>
                                <p><b>Población: </b>'.$localidad.'</p>
                                <p><b>CIF/DNI: </b>'.$cif_nif.'</p>
			</td>
			<td colspan="2">
				<p><b>Fecha:  </b>'.date("d") . "-" . date("m") . "-" . date("Y").'</p>
				<p><b>Marca/Modelo: </b>'.$marca.'</p>
				<p><b>Matrícula: </b>'.$matricula.'</p>
                                <p><b>Kms: </b>'.$kms.'</p>    
				<p><b>Chasis: </b>'.$bastidor.'</p>
				
			</td>
		</tr>
	</table>
	<table>
		<tr>
			<td colspan="2"><b>CONCEPTOS</b></td>
			<td colspan="2,5"><b>Fecha Entrada: </b>'.date("d-m-Y", strtotime($entrada)).'</td>
			<td colspan="3"><b>Fecha Salida: </b>'.date("d-m-Y", strtotime($salida)).'</td>
		</tr>
		<tr>
			<td colspan="7">'.$descripcion.'</td>
		</tr>
		<tr>
			<th width="25px">Código</th>
			<th colspan="2">Descripción</th>
			<th width="10px">DTO.</th>
			<th width="25px">CANT.</th>
			<th width="75px">PVP</th>
			<th width="75px">TOTAL</th>
		</tr>';
 if (!empty($resultado_detalle)) {
        $contador = 0;
        $base = 0;
        foreach ($resultado_detalle as $valor) {
           
            $subtotal = number_format(($valor['importe']*$valor['cantidad'])-(($valor['importe']*$valor['cantidad'])*($valor['dto']/100)),2,'.','');
            $base +=  $subtotal;
            $contador ++;
            $contenido .= '<tr  class="row-det">
			<td>'.$valor['codigo'].'</td>
			<td colspan="2">'.$valor['desc_repuesto'].'</td>
			<td align="center">'.$valor['dto'].'</td>
			<td align="center">'.$valor['cantidad'].'</td>
			<td align="right">'.$valor['importe'].'</td>
			<td align="right">'.$subtotal.'</td>
		</tr>';
            
            }
 }
          $dto_pie = number_format(($base*$dto)/100,2,'.','');
          $subtotal_pie = $base - $dto_pie;
          $iva_pie =  number_format(($subtotal_pie * $iva/100),2,'.','');
          $total = number_format(($subtotal_pie + $iva_pie),2,'.','');
          
          for ($i=$contador;$i<17;$i++){
               $contenido .= '<tr class="row-tr">
			<td></td>
			<td colspan="2"></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
		</tr>';
          }      
$contenido .= 		
		'<tr>
			<td colspan="4" valign="top"><b>Fecha y Firma</b></td>
			<td colspan="3">
				<p><b>BASE: </b><span align="right">'.number_format($base,2,'.','').' €</span></p>
				<p><b>DTO %: </b>'.$dto_pie.' €</p>
				<p><b>SUBTOTAL: </b>'.$subtotal_pie.' €</p>
				<p><b>I.V.A. 21%: </b>'.$iva_pie.' €</p>
				<p><b>TOTAL: </b>'.$total.' €</p>
			</td>
		</tr>	
	</table>
        <p style="text-align:justify;font-size:8px">José Isidoro Vidal es el Responsable del tratamiento de los datos personales proporcionados bajo su 
        consentimiento y le informa que estos datos serán tratados de conformidad con lo dispuesto en el 
        Reglamento (UE) 2016/679 de 27 de abril de 2016 (GDPR), con la finalidad de mantener una relación comercial
        y conservarlos mientras exista un interés mutuo para mantener el fin del tratamiento y cuando ya no sea 
        necesario para tal fin, se suprimirán con medidas de seguridad adecuadas para garantizar la seudonimización
        de los datos o la destrucción total de los mismos. No se comunicarán los datos a terceros, salvo obligación
        legal. Asimismo, se informa que puede ejercer los derechos de acceso, rectificación, portabilidad y 
        supresión de sus datos y los de limitación y oposición a su tratamiento dirigiéndose a José Isidoro Vidal
        en Bº La Trapa, S/N, 39718, San Vitores, Medio Cudeyo(Cantabria)</p>
</body>
</html>';
               
        $mpdf = NEW Mpdf([
            'mode' => 'utf-8',
//            "format" => "A4-L",
    //           
//                "margin_bottom" => 3,
//                "margin_top" => 3,
    //            "margin_footer" => 0
        ]);
        
        $css = file_get_contents('../web/css/factura.css');
     
        $mpdf->WriteHTML($css, \Mpdf\HTMLParserMode::HEADER_CSS);
        $mpdf->WriteHTML($contenido, \Mpdf\HTMLParserMode::HTML_BODY);
//        $mpdf->AddPage();
//        $mpdf->WriteHTML($css, \Mpdf\HTMLParserMode::HEADER_CSS);
//        $mpdf->WriteHTML($contenido_pag2,\Mpdf\HTMLParserMode::HTML_BODY); 
            $mpdf->Output(); 
           
               
    }
    
}
