<?php

use yii\helpers\Html;
use yii\data\ActiveDataProvider;
use app\models\vehiculos;
/* @var $this yii\web\View */
/* @var $model app\models\Clientes */

$this->title = 'Nuevo Cliente';
$this->params['breadcrumbs'][] = ['label' => 'Clientes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$mensaje = "";
$vehiculo = new vehiculos();                        
$consulta = $vehiculo->find()
->where(['cliente'=>$model->id]);

$dataProvider = new ActiveDataProvider([
    'query' => $consulta,
    'pagination' => false,
]);

?>
<div class="clientes-update">

    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
        'dataProvider' => $dataProvider,
        'mensaje' => $mensaje
    ]) ?>

</div>
