<?php

use yii\helpers\Html;
use yii\data\ActiveDataProvider;
use app\models\parte2;

/* @var $this yii\web\View */
/* @var $model app\models\Parte1 */

//$this->title = 'Nuevo Parte';
$this->params['breadcrumbs'][] = ['label' => 'Partes', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Nuevo Parte';

if(isset($vehiculo)){
    $codigo_vehiculo = $vehiculo;
}else{
    $codigo_vehiculo = "";
}

if(isset($datos_autocomplete)){
    $datos_complete = $datos_autocomplete;
}else{
    $datos_complete = "";
}


$mensaje = "";
$detalle = new parte2();                        
$consulta = $detalle->find()
->where(['id_parte1'=>$model->id]);

$dataProvider = new ActiveDataProvider([
    'query' => $consulta,
    'pagination' => false,
]);

?>
<div class="parte1-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'dataProvider' => $dataProvider,
        'vehiculo' => $codigo_vehiculo,
        'datos_autoc' => $datos_complete,
        'mensaje' => $mensaje
    ]) ?>

</div>